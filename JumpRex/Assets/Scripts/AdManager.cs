﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour, IUnityAdsListener
{
    [Header("Reward Manager")]
    public PlayerManager playerManager;
    public GameObject lostCanvas;

    [Header("Ads Manager")]
    public string gameId;
    public string placementId;
    public string placement;
    public bool testMode;

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        StartCoroutine(ShowBanner());
    }

    IEnumerator ShowBanner()
    {
        Advertisement.Initialize(gameId, testMode);

        while (!Advertisement.IsReady(placementId))
        {
            yield return null;
        }

        Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
        Advertisement.Banner.Show(placementId);
    }

    public void ShowAd()
    {
        StartCoroutine(ShowVideoAd());
    }

    IEnumerator ShowVideoAd()
    {
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, true);

        while (!Advertisement.IsReady(placement))
        {
            yield return null;
        }

        Advertisement.Show();
    }

    public void OnUnityAdsReady(string placementId)
    {
        
    }

    public void OnUnityAdsDidError(string message)
    {
        
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        //throw new System.NotImplementedException();

        if(showResult == ShowResult.Finished)
        {
            lostCanvas.SetActive(false);
            playerManager.Continue();
        }

        else if (showResult == ShowResult.Failed)
        {
            //Não implementei lógica, mas aqui colocar o que acontece se o video não tocar
        }
    }
}