﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Vector3 newPos;
    public float speed;
    public bool moveCamera;

    void Start()
    {
        
    }

    void Update()
    {
        if (moveCamera)
        {
            transform.position = Vector3.MoveTowards(transform.position, newPos, speed);

            if (transform.position.y >= newPos.y)
            {
                moveCamera = false;
            }
        }
    }

    public void Calculate()
    {
        newPos = transform.position;
        newPos.y += 6f;
        moveCamera = true;
    }
}