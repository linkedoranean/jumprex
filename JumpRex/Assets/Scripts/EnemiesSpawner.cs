﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesSpawner : MonoBehaviour
{
    public GameObject[] spawners;
    public float timer;
    public float baseTime;

    void Start()
    {
        
    }

    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0)
        {
            StartCoroutine(SpawnBullets());
            timer = baseTime;
        }
    }

    IEnumerator SpawnBullets()
    {
        WaitForSeconds wait = new WaitForSeconds(0.1f);
        foreach (GameObject spawner in spawners)
        {
            spawner.transform.GetChild(0).gameObject.SetActive(true);
            yield return wait;
        }
    }
}