﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool mainMenu;

    [Header("Canvas Manager")]
    public Canvas mainCanvas;
    public Animator canvasAnim;

    [Header("Stage Building")]
    public Vector2 screenBounds;
    public GameObject leftLimit;
    public GameObject rightLimit;
    public GameObject floor;
    public float floorPosition;
    public float newX;
    public float offset;

    [Header("Main Menu Manager")]
    public int sceneToLoad;

    [Header("Pipes Manager")]
    public GameObject[] pipes;

    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

        if (!mainMenu)
        {
            foreach (GameObject pipe in pipes)
            {
                if (pipe.transform.position.x > 0)
                {
                    newX = screenBounds.x - offset;
                    pipe.transform.localPosition = new Vector3(newX, pipe.transform.localPosition.y, pipe.transform.localPosition.z);
                }

                if (pipe.transform.position.x < 0)
                {
                    newX = (screenBounds.x * -1) + offset;
                    pipe.transform.localPosition = new Vector3(newX, pipe.transform.localPosition.y, pipe.transform.localPosition.z);
                }
            }

            leftLimit.transform.position = new Vector3(screenBounds.x * -1, leftLimit.transform.position.y, leftLimit.transform.position.z);
            rightLimit.transform.position = new Vector3(screenBounds.x, rightLimit.transform.position.y, rightLimit.transform.position.z);

        }
        /*Usado para criar o cenário dinâmicamente
        for (int i = 0; i < 20; i++)
        {
            Instantiate(floor, new Vector3(0, floorPosition, 0), Quaternion.identity);
            floorPosition += 1.5f;
        }*/
    }

    public void LoadStage()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}