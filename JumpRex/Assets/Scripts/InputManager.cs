﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [Header("Getting Player Touches")]
    Camera cam;
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    Vector2 mouseWorldPosition;

    [Header("Player Manager")]
    public PlayerManager playerManager;

    [Header("Animation Manager")]
    public Animator animManager;

    public enum PlayerState { idle, running, jumping, dead};
    public PlayerState playerState;

    void Start()
    {
        cam = Camera.main;
        playerManager = FindObjectOfType<PlayerManager>();
    }

    void Update()
    {
        if (!playerManager.dead && !playerManager.gameOver)
        {
            if (Input.GetMouseButtonDown(0))
            {
                RegisterFirstTouch();
            }

            if (Input.GetMouseButtonUp(0))
            {
                Swipe();
            }

            KeyboardInput();
        }
    }

    public void RegisterFirstTouch()
    {
        mouseWorldPosition = cam.ScreenToWorldPoint(Input.mousePosition);

        firstPressPos = new Vector2(mouseWorldPosition.x, mouseWorldPosition.y);
    }



    public void Swipe()
    {
        mouseWorldPosition = cam.ScreenToWorldPoint(Input.mousePosition);

        secondPressPos = new Vector2(mouseWorldPosition.x, mouseWorldPosition.y);

        currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

        currentSwipe.Normalize();

        //swipe up
        if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            MoveUp();
        }

        //swipe down
        if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            MoveDown();
        }

        //swipe right
        if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            MoveRight();
        }

        //swipe left
        if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            MoveLeft();
        }
    }

    public void KeyboardInput()
    {


        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MoveUp();
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MoveDown();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveRight();
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MoveLeft();
        }
    }

    public void MoveUp()
    {
        if (playerManager.isGrounded)
        {
            animManager.SetTrigger("Jump");
        }

        if (playerState == PlayerState.running)
        {
            animManager.SetTrigger("JumpRun");
        }

        if (playerState == PlayerState.idle)
        {
            animManager.SetTrigger("JumpStop");
        }

        playerManager.Jump();
    }

    public void MoveDown()
    {
        if (playerState != PlayerState.idle && playerManager.isGrounded)
        {
            animManager.SetTrigger("Stop");
            playerState = PlayerState.idle;
            playerManager.Stop();
        }
    }

    public void MoveRight()
    {
        if (playerManager.canGoRight)
        {
            playerState = PlayerState.running;
            animManager.SetTrigger("Run");

            if (playerManager.direction == -1)
            {
                playerManager.ChangeDirection();
            }
            playerManager.direction = 1;
            playerManager.walk = true;
        }
    }

    public void MoveLeft()
    {
        if (playerManager.canGoLeft)
        {
            playerState = PlayerState.running;
            animManager.SetTrigger("Run");

            if (playerManager.direction == 1)
            {
                playerManager.ChangeDirection();
            }
            playerManager.direction = -1;
            playerManager.walk = true;
        }
    }
}