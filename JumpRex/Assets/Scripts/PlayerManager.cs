﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [Header("Ground Manager")]
    public bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatisGround;

    [Header("Walk Manager")]
    public float speed;
    public bool walk;
    public bool invertDirection;
    public bool canGoRight;
    public bool canGoLeft;

    [Header("Jump Manager")]
    public float jumpForce;
    public bool canJump;
    public float jumpHeight;
    public float jumpTimer;

    [Header("Rigidbody Manager")]
    public float direction;
    Rigidbody2D rb;

    [Header("Input Manager")]
    public InputManager inputManager;

    [Header("Fruit Manager")]
    public int collectedFruit;

    [Header("Health Manager")]
    public bool dead;
    public BoxCollider2D playerCollider;

    [Header("Game Manager")]
    public bool gameOver;
    public GameObject lostButtons;
    public GameObject wonButtons;

    void Start()
    {
        playerCollider = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        inputManager = FindObjectOfType<InputManager>();
    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatisGround);

        if (walk)
        {
            if (direction > 0 && canGoRight ||
                direction < 0 && canGoLeft)
            {
                rb.constraints = RigidbodyConstraints2D.None;
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                rb.velocity = new Vector2(direction * speed, rb.velocity.y);
            }
        }

        if (canJump)
        {
            if (jumpTimer > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, 1 * jumpForce);
                jumpTimer -= Time.deltaTime;
            }
            if (jumpTimer < 0)
            {
                canJump = false;
            }
        }
    }

    public void ChangeDirection()
    {
        invertDirection = !invertDirection;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }

    public void Jump()
    {
        if (isGrounded)
        {
            rb.constraints = RigidbodyConstraints2D.None;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;

            jumpTimer = jumpHeight;
            canJump = true;
        }
    }

    public void Stop()
    {
        walk = false;
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public void Continue()
    {
        dead = false;
        inputManager.animManager.SetTrigger("Revive");
        playerCollider.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Limit" && collision.transform.localPosition.x > transform.localPosition.x)
        {
            rb.constraints = RigidbodyConstraints2D.FreezePositionX;
            inputManager.MoveDown();
            canGoRight = false;
        }

        if (collision.tag == "Limit" && collision.transform.localPosition.x < transform.localPosition.x)
        {
            rb.constraints = RigidbodyConstraints2D.FreezePositionX;
            inputManager.MoveDown();
            canGoLeft = false;
        }

        if (collision.tag == "Fruit")
        {
            collectedFruit++;
            collision.gameObject.SetActive(false);
        }

        if (collision.tag == "SpecialFruit")
        {
            wonButtons.SetActive(true);
            collectedFruit++;
            collision.gameObject.SetActive(false);
            gameOver = true;
            Stop();
            inputManager.animManager.SetTrigger("Stop");
            canJump = false;
            rb.constraints = RigidbodyConstraints2D.None;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        }

        if (collision.tag == "Bullet")
        {
            lostButtons.SetActive(true);
            Stop();
            dead = true;
            canJump = false;
            inputManager.animManager.SetTrigger("Dead");
            collision.transform.parent.gameObject.SetActive(false);
            rb.constraints = RigidbodyConstraints2D.None;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        }

        if (collision.tag == "CameraMover")
        {
            collision.transform.parent.GetComponent<CameraManager>().Calculate();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!canGoRight && direction < 0)
        {
            rb.constraints = RigidbodyConstraints2D.None;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            canGoRight = true;
        }

        if (!canGoLeft && direction > 0)
        {
            rb.constraints = RigidbodyConstraints2D.None;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            canGoLeft = true;
        }
    }
}