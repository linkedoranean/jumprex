﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeBallManager : MonoBehaviour
{
    public GameObject parent;
    public float speed;

    void Start()
    {
        
    }

    void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" ||
            collision.tag == "Limit")
        {
            //transform.parent = parent.transform;
            gameObject.SetActive(false);
            transform.localPosition = Vector3.zero;
        }
    }
}